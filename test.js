import {sleep} from 'k6';
import http from 'k6/htpp';

export let options= {
    duration: '2m',
    vus: 200,
}

export default function () {
    http.get('husky-stove.surge.sh')
    sleep(1);
}
